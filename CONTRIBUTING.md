# Contributing

Please post issues on <https://framagit.org/luc/dolomon/issues>.

If you want to add a language to dolomon, edit `Makefile` (take exemple on EN and FR) and do `make locales`.
It will create a .po file for your language, waiting for your translation, modify it then commit it and create a merge request.
